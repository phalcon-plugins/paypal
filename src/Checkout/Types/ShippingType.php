<?php

namespace PMP\Plugins\PayPal\Checkout\Types;

use PMP\Plugins\PayPal\Checkout\Types\BaseType;

/**
 * ShippingType
 *
 * @author Dimitar Stanimirov <stanimirov.dimitar@gmail.com>
 */
class ShippingType extends BaseType {

    /**
     * @var \PMP\Plugins\PayPal\Checkout\Types\NameType
     */
    var $name;

    /**
     * @var PMP\Plugins\PayPal\Checkout\Types\AddressType
     */
    var $address;

}
