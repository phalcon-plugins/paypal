<?php

namespace PMP\Plugins\PayPal\Checkout\Constants;

/**
 * CurrenciesConstants
 *
 * @author Dimitar Stanimirov <stanimirov.dimitar@gmail.com>
 */
class CurrenciesConstants {

    /**
     * @var string
     */
    const USD = 'USD';

}
